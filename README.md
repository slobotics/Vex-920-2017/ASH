# ASH
The code behind Jake, Alyssa, and Ganden's bot.

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
